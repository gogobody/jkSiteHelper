<?php

namespace TypechoPlugin\jkSiteHelper\classes;
use Utils\Helper;

define('download_path', __TYPECHO_ROOT_DIR__.'/usr/jksitetool_download_tmp');
require_once jksite_basepath.'/core/libs/vendor/autoload.php';

class Downloader
{
    public static function checkDirExist()
    {
        if (!file_exists(download_path)){
            @mkdir(download_path, 0777, true);
            @chmod(download_path, 0777);
        }

    }

    public static function download_file($url)
    {
        self::checkDirExist();
        $data = [];
        $data['errno'] = 0;
        $data['path'] = '';

        if ($url){
            $file = @file_get_contents($url);
            if (!$file) {
                $data['errno'] = 1;
                $data['message'] = '下载失败';
                return $data;
            }
        }

        $pathArr    = explode('/', $url);
        $fileName   = array_pop($pathArr);
        $pathInfo = pathinfo($url);
        if (!isset($pathInfo['extension'])) {
            $data['errno'] = 1;
            $data['message'] = '扩展名错误';
            return $data;
        }
        $local_file_name = download_path.'/'.$fileName;

        if (file_put_contents($local_file_name, $file)) {
            $data['path'] = $local_file_name;
        }
        return  $data;
    }

    public static function unzip($outputFilename, $outputDirExtract)
    {
        $zipFile = new \PhpZip\ZipFile();
        try {
            $zipFile
                ->openFile($outputFilename)
                ->extractTo($outputDirExtract);
            return true;
        }catch(\PhpZip\Exception\ZipException $e){
            // handle exception
            var_dump($e);
        }
        finally{
            $zipFile->close();
        }
        return false;
    }
}
