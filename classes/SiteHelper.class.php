<?php
namespace TypechoPlugin\jkSiteHelper\classes;
use Typecho\Plugin;
use Typecho\Plugin\Exception;
use Utils\Helper;
use Widget\Options;

require_once \Utils\Helper::options()->pluginDir('jkSiteHelper').'/functions/defines.php';
require_once 'Common.class.php';

class SiteHelper
{
    private static $instance;

    public static $optionsName = 'jkSiteHelper';

    public static $plugins= array();

    public function __construct(){}


    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function buildOptionsMenu()
    {
        if (class_exists('CSF')){
            [$pluginFileName, $className] = Plugin::portal('jkSiteHelper', __TYPECHO_ROOT_DIR__.__TYPECHO_PLUGIN_DIR__);
            $main_plugin_info = Plugin::parseInfo($pluginFileName);
            $main_plugin_version = $main_plugin_info['version'];


            $html = "<link rel='stylesheet' href='".jksite_baseurl."/assets/css/main.min.css'><div class='jksite-loading'></div>
            <div class='typecho-table-wrap' style='padding-bottom: 0'><strong style='font-size: large'>当前插件版本：{$main_plugin_version}</strong></div>
            <div class='typecho-table-wrap' style='padding-bottom: 0'><button type='button' class='button button-primary csf-top-save csf-save csf-save-ajax' onclick='check_jkUpdate()'>检查更新</button></div>
            <div>
                <div class='typecho-table-wrap'>
                <h3>内置插件</h3>
                    <table class='typecho-list-table'>
                    <colgroup>
                            <col width='5%' class='kit-hidden-mb'>
                            <col width='25%' class='kit-hidden-mb'>
                            <col width='50%'>
                            <col width='20%'>
                        </colgroup>
                    <thead>
                    <th><th>插件</th><th>描述</th><th>操作</th></th>
</thead>
<tbody id='outer-tools'>
</tbody>
</table>
</div>
 <div class='typecho-table-wrap'>
                <h3>独立插件</h3>
                    <table class='typecho-list-table'>
                    <colgroup>
                            <col width='5%' class='kit-hidden-mb'>
                            <col width='25%' class='kit-hidden-mb'>
                            <col width='50%'>
                            <col width='20%'>
                        </colgroup>
                    <thead>
                    <th><th>插件</th><th>描述</th><th>操作</th></th>
</thead>
<tbody id='standalone_tools'></tbody>
</table>
</div>
</div>";
            // create options
            $prefix = self::$optionsName;
            \CSF::createOptions( $prefix, array(
                'menu_title' => 'SiteHelper',
                'menu_slug'  => 'sitehelper',
            ) );
            \CSF::createSection( $prefix, array(
                'title'  => '中控台',
                'fields' => array(
                    array(
                        'type'    => 'submessage',
                        'style'   => 'success',
                        'content' => '欢迎使用即刻站长工具箱！交流群：1044509220',
                    ),
                    array(
                        'type'    => 'content',
                        'content' => $html,
                    ),
//                    array(
//                        'id'    => 'opt-tabbed-1',
//                        'type'  => 'tabbed',
//                        'tabs'  => array(
//
//                            array(
//                                'title'  => 'Tab 1',
//                                'fields' => array(
//                                    array(
//                                        'id'    => 'opt-text-1',
//                                        'type'  => 'text',
//                                        'title' => 'Text 1',
//                                    ),
//                                    array(
//                                        'id'    => 'opt-textarea-1',
//                                        'type'  => 'textarea',
//                                        'title' => 'Textarea 1',
//                                    ),
//                                ),
//                            ),
//
//                            array(
//                                'title'  => 'Tab 2',
//                                'fields' => array(
//                                    array(
//                                        'id'    => 'opt-text-2',
//                                        'type'  => 'text',
//                                        'title' => 'Text 2',
//                                    ),
//                                    array(
//                                        'id'    => 'opt-textarea-2',
//                                        'type'  => 'textarea',
//                                        'title' => 'Textarea 2',
//                                    ),
//                                ),
//                            ),
//
//                        ),
//                    ),
                )
            ));
            \CSF::createSection( $prefix, array(
                'title'  => '内置工具',
                'id' => 'builtintools'
            ));
            \CSF::createSection( $prefix, array(
                'title'  => '外部工具',
                'id' => 'builtouttools'
            ));
            // require builtin tools

            self::loadBuiltinTools(jksitehelper_plugin_path('models/builtin/'),'Plugin.php', ['parent_id'=>'builtintools']);

            // load builtout tools
            self::loadBuiltinTools(jksitehelper_plugin_path('models/builtout/'),'Plugin.php', ['parent_id'=>'builtouttools']);

        }

    }

    /**
     * @throws Exception
     */
    public static function loadBuiltinTools($directory, $afterfix='.class.php', $args = [], $depth=0)
    {
        if ($depth == 2){
            return;
        }

        if(is_dir($directory)) {
            $scan = scandir($directory);
            unset($scan[0], $scan[1]); //unset . and ..

            foreach($scan as $file) {
                if(is_dir($directory."/".$file)) {
                    self::loadBuiltinTools($directory."/".$file, $afterfix ,$args,$depth+1);
                } else {

                    if(strpos($file, $afterfix) !== false) {
                        $ns = require_once($directory."/".$file);
                        if (!is_string($ns)){
                            // 一些插件卸载时出现问题，例如tpcache,直接跳过
                            if (strpos($_SERVER['REQUEST_URI'],'deactivate')!==false){
                                continue;
                            }
                            throw new Exception('请检查插件Plugin 是否返回NAMESPACE，file: '.$directory."/".$file);
                        }
                        $classname = $ns.'\Plugin';
                        if (array_key_exists('parent_id' , $args)){
                            if (method_exists($classname, 'config')){
                                $classname::config($args['parent_id']);
                                self::$plugins[$classname] = [
                                    'version' => $classname::Version
                                ]; // todo: add plugin info
                            }
                        }
                    }
                }
            }
        }
    }

}
