<?php
namespace TypechoPlugin\jkSiteHelper\classes;


class Common
{
    public static function isInPluginConfig(): bool
    {
        $requestObject = \Typecho\Request::getInstance();
        $curUri = $requestObject->getRequestUri();
        return strpos($curUri, 'config=jkSiteHelper')!==false;
    }

    public static function getRemoteUpdateInfos()
    {
        $content = file_get_contents('https://gogobody.gitee.io/jksitehelper/data/version.json');
        $json = json_decode($content);
        var_dump($json);
    }
}
