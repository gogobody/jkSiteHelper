<?php

use \Utils\Helper;

define("jksite_basepath", Helper::options()->pluginDir.'/jkSiteHelper');
define("jksite_baseurl", \Typecho\Common::url( 'jkSiteHelper',Helper::options()->pluginUrl));

function jksitehelper_plugin_path($file)
{
    return \Utils\Helper::options()->pluginDir('jkSiteHelper').DIRECTORY_SEPARATOR.$file;
}

if (!function_exists('load_csf')){
    function load_csf(){
        if (!class_exists('CSF')) {
            require_once Helper::options()->pluginDir('jkOptionsFramework') . '/jkoptions-framework.php';
        }
    }
}

if (!function_exists('load_definedphp')){
    function load_definedphp($directory, $afterfix='.class.php', $args = []) {
        if(is_dir($directory)) {

            $scan = scandir($directory);
            unset($scan[0], $scan[1]); //unset . and ..
            foreach($scan as $file) {

                if(is_dir($directory."/".$file)) {
                    load_definedphp($directory."/".$file, $afterfix ,$args);
                } else {
                    if(strpos($file, $afterfix) !== false) {
                        $ns = require_once($directory."/".$file);
                        $classname = $ns.'\Plugin';
                        if (array_key_exists('parent_id' , $args)){
                            $classname::config($args['parent_id']);
                        }
                    }
                }
            }
        }
    }
}

// 无法使用
//if (!function_exists('restart_plugin')){
//    function restart_plugin($pluginName){
//        Helper::removePlugin($pluginName);
//        @Typecho\Plugin::activate($pluginName);
//    }
//}
