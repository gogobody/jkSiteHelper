<?php

namespace TypechoPlugin\jkSiteHelper;

use Typecho\Widget;
use TypechoPlugin\jkSiteHelper\classes\Downloader;
use Utils\Helper;
use Widget\User;
require_once 'functions/defines.php';
require_once 'classes/Downloader.php';

class Action extends Widget implements \Widget\ActionInterface
{

    public function action()
    {

    }

    public function download_plugin()
    {
        $data = [
            'errno' => 0,
            'message' => '下载成功'
        ];
        if (!User::alloc()->hasLogin()){
            $data['errno'] = 1;
            $data['message'] = '用户未登录';
            $this->response->throwJson($data);
        }
        $plugin_url = $_POST['url'];
        if ($plugin_url){
            $ret = Downloader::download_file($plugin_url);
            if ($ret['errno'] == 0){
                $ret = Downloader::unzip($ret['path'],jksite_basepath.'/models/builtout');
                if ($ret){
                    // 解压成功
                    $this->response->throwJson($data);
                } else {
                    // 解压失败
                    $data['errno'] = 1;
                    $data['message'] = '插件 '.$plugin_url.' 解压失败！';
                }
            }else {
                $data['errno'] = 1;
                $data['message'] = $ret['message'];
            }
        } else {
            $data['errno'] = 1;
            $data['message'] = '没有 url';
        }
        $this->response->throwJson($data);
    }

}
