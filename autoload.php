<?php


use Typecho\Exception;
use TypechoPlugin\jkSiteHelper\classes\SiteHelper;
use Utils\Helper;

$plugins = \Typecho\Plugin::export();
$activatedPlugins = $plugins['activated'];
if(!array_key_exists('jkOptionsFramework', $activatedPlugins)){
    throw new Exception('jkOptionsFramework 插件未启用！请查看 https://github.com/gogobody/jkOptionsFramework');
}

if(!class_exists('CSF')){
    require_once Helper::options()->pluginDir('jkOptionsFramework').'/jkoptions-framework.php';
}
if (!class_exists('jkOptions')){
    require_once \Utils\Helper::options()->pluginDir('jkOptionsFramework').'/jkOptions.php';
}
// set global vars
$GLOBALS['wpdb'] = \Typecho\Db::get();

require_once 'classes/SiteHelper.class.php';
SiteHelper::getInstance();
SiteHelper::buildOptionsMenu();

if (file_exists(__DIR__ . '/config.php')) {
    require_once __DIR__ . '/config.php';
}
