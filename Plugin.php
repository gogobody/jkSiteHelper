<?php

namespace TypechoPlugin\jkSiteHelper;
if (!defined('__TYPECHO_ROOT_DIR__')) exit;

require_once 'autoload.php';

use Typecho\Exception;
use Typecho\Plugin\PluginInterface;
use TypechoPlugin\jkOptionsFramework;
use TypechoPlugin\jkSiteHelper\classes\SiteHelper;
use Utils\Helper;


/**
 * jkSiteHelper 是 Typecho 的一款站长工具箱
 * <div class="jkSiteHelperStyle"><a style="width:fit-content" id="jkSiteHelper">您目前使用的是最新版</div>&nbsp;</div><style>.jkSiteHelperStyle{margin-top: 5px;}.jkSiteHelperStyle a{font-size: smaller;background: #4DABFF;padding: 5px;color: #fff;}</style>
 * <script>var jkhelperversion="1.0.3";function jkhelper_update(){localStorage.setItem("jkhelper_update",(new Date()).getHours());var container=document.getElementById("jkSiteHelper");console.log(container);if(!container){return};var ajax=new XMLHttpRequest();container.style.display="block";ajax.open("get","https://gitee.com/api/v5/repos/gogobody/jkSiteHelper/releases/latest");ajax.send();ajax.onreadystatechange=function(){if(ajax.readyState===4&&ajax.status===200){var obj=JSON.parse(ajax.responseText);var newest=obj.tag_name;if(newest>jkhelperversion){container.innerHTML="发现新版本："+obj.tag_name+'。下载地址：<a href="https://gitee.com/gogobody/jkSiteHelper/repository/archive/master.zip">点击下载</a>'+"<br>您目前的版本:"+String(jkhelperversion)+"。"}else{container.innerHTML="您目前使用的是最新版。"}}}};if(Math.abs(localStorage.getItem("jkhelper_update") - (new Date()).getHours())>1){jkhelper_update();}</script>

 * @package jkSiteHelper
 * @author gogobody
 * @version 1.0.3
 * @link https://www.ijkxs.com
 *
 */
class Plugin implements PluginInterface
{
    public static $optionsName = 'jkSiteHelper';


    /**
     * @throws Exception
     */
    public static function activate()
    {
        if (version_compare(PHP_VERSION, '7.4', '<')){
            throw new Exception('插件所需最低PHP版本为 7.4');
        }
        if (!self::checkDependency()) {
            throw new Exception('jkOptionsFramework 插件未启用！请查看 https://github.com/gogobody/jkOptionsFramework');
        }

        foreach (SiteHelper::$plugins as $classname => $val){
            if(method_exists($classname,'activate')) {
                $classname::activate();
            }
        }
        Helper::addRoute('download_plugin', '/download_jk_file','TypechoPlugin\jkSiteHelper\Action','download_plugin');
    }



    public static function deactivate()
    {
        foreach (SiteHelper::$plugins as $classname => $val){
            if(method_exists($classname,'deactivate')){
                $classname::deactivate();
            }
        }
        Helper::removeRoute('download_plugin');
    }

    public static function config(\Typecho\Widget\Helper\Form $form)
    {
        if (!class_exists('CSF')) {
            require_once \Utils\Helper::options()->pluginDir('jkOptionsFramework') . '/jkoptions-framework.php';
        }
        $params = [
            'args' => [
                'framework_title' => 'jkSiteHelper插件设置',
                'footer_text' => '感谢您使用jkSiteHelper插件',
            ]
        ];
        \CSF::setup(self::$optionsName, $params);
        require('views/plugin_load.php');
        return \CSF::setTypechoOptionForm($form);
    }

    public static function personalConfig(\Typecho\Widget\Helper\Form $form)
    {
        // TODO: Implement personalConfig() method.
    }
    public static function configHandle($settings,$isSetup)
    {
        foreach (SiteHelper::$plugins as $classname => $val){
            if(method_exists($classname,'configHandle')){
                $classname::configHandle($settings,$isSetup);
            }
        }
        Helper::configPlugin('jkSiteHelper', $settings);
    }
    public static function checkDependency()
    {
        $plugins = \Typecho\Plugin::export();
        $activatedPlugins = $plugins['activated'];
        return array_key_exists('jkOptionsFramework', $activatedPlugins);
    }
}
