## 即刻站长工具箱

一个适用于Typecho的站长工具箱


## 需求
插件需要 typecho 1.2   
php版本最低 7.4

## 主要功能：
- 各种评论过滤
- 各种seo推送：百度，bing，360，神马
- 静态缓存：redis，memcache

## 使用：
此插件使用 [即刻选项框架](https://github.com/gogobody/jkOptionsFramework)，因此，请先安装该插件。

然后下载jkSiteHelper最新版，放入插件目录，确保插件文件夹名为jkSiteHelper
