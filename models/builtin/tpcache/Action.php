<?php
namespace TypechoPlugin\jkSiteHelper\models\builtin\tpcache;
use jkOptions;
use \Utils\Helper;
use Typecho\{Exception, Widget, Db};
use Typecho\Cookie;

if (!class_exists('jkOptions')){
    require_once \Utils\Helper::options()->pluginDir('jkOptionsFramework').'/jkOptions.php';
}

class Action extends Widget implements \Widget\ActionInterface
{

    public function action(){

    }


    public function clean_cache()
    {
        $ins = jkOptions::getInstance();
        $options = $ins::get_option( 'jkSiteHelper' );
        $config = $options['tpcache'];
        if ($config['cache_driver'] != '0') {
            Plugin::initBackend($config['cache_driver']);
            try {
                Plugin::$cache->flush();
                echo '清理成功';
            } catch (Exception $e) {
                print $e->getMessage();
            }
        } else{
            // 关闭其他插件选项
            if ($config['enable_gcache'] == '1'){
                $config['enable_gcache'] = 0;
            }
            if ($config['enable_markcache'] == '1'){
                $config['enable_markcache'] = 0;
            }
            echo '未启用缓存';
        }

        $options['tpcache'] = $config;
        jkOptions::update_option('jkSiteHelper', $options);

    }
}
