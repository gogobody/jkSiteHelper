<?php
use Typecho\Http\Client;
require_once 'SEO_Base.class.php';

class SEO_SM extends SEO_Base {
    /**
     * @throws Exception
     */
    public static function push($url)
    {
        $ins = jkOptions::getInstance();
        $options = $ins::get_option( 'jkSiteHelper' );

        $push_url = $options['seotools']['sm_push_url'];
        //判断是否配置好API
        if (is_null($push_url)) {
//            throw new Exception('百度推送未配置');
            return;
        }

        //准备数据
        if (is_array($url)) {
            $urls = $url;
        } else {
            $urls = array($url);
        }

        //日志信息
        $log['subject'] = '我';
        $log['action'] = '神马收录API推送';
        $log['object'] = implode(",", $urls);

        try {
            //为了保证成功调用，老高先做了判断
            if (!Client::get()) {
                throw new \Typecho\Plugin\Exception(_t('对不起, 您的主机不支持 php-curl 扩展而且没有打开 allow_url_fopen 功能, 无法正常使用此功能'));
            }

            //发送请求
            $http = Client::get();
            $http->setData(implode("\n", $urls));
            $http->setHeader('Content-Type', 'text/plain');
            $http->send($push_url);
            $json = $http->getResponseBody();
            $return = json_decode($json, 1);
            $log['json'] = $return['errorMsg'];

            if (isset($return['returnCode']) and $return['returnCode']=='200') {
                $log['result'] = '成功';
            } else {
                $log['result'] = '失败';
            }
        } catch (\Typecho\Exception $e) {
            $log['result'] = '失败：' . $e->getMessage();
        }

        self::logger($log);
    }

}
