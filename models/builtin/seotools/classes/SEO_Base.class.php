<?php

class SEO_Base
{
    public static function logger($data)
    {
        $log_dir = __TYPECHO_ROOT_DIR__ . '/usr/log/';
        if (!is_dir($log_dir)) @mkdir($log_dir, 0777);//检测缓存目录是否存在，自动创建
        $ins = jkOptions::getInstance();
        $jkoptions = $ins::get_option( 'jkSiteHelper' );
        $api_token = $jkoptions['seotools']['enable_log'];
        if ($api_token == '1'){
            //将日志记录写入文件中
            $row = date('Y-m-d H:i:s') . "\t" . $data['subject'] . "\t" . $data['action'] . "\t" . $data['object'] . "\t" . $data['result'] ."\t" . $data['json'] . "\t" . "\r\n";
            @error_log($row, 3, $log_dir . date('Ymd') . '.log');
        }

    }
}
