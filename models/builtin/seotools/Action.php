<?php
namespace TypechoPlugin\jkSiteHelper\models\builtin\seotools;
use jkOptions;
use \Utils\Helper;
use \Typecho\{Widget, Db};
use Typecho\Cookie;

if (!class_exists('jkOptions')){
    require_once \Utils\Helper::options()->pluginDir('jkOptionsFramework').'/jkOptions.php';
}

class Action extends Widget implements \Widget\ActionInterface
{
    public function action(){

    }

    public function sitemap_api()
    {
        $ins = jkOptions::getInstance();
        $jkoptions = $ins::get_option( 'jkSiteHelper' );
        $api_token = $jkoptions['seotools']['api_token'];
        //检查api token
        if ($api_token == null) {
            echo 'api token 未配置';
            exit;
        }
        if (isset($_GET['token']) && $_GET['token'] === $api_token) {
            $array = array();
            if (isset($_GET['sitemap']) && $_GET['sitemap'] === 'update') {
                //add arr
                $arr = array(
                    'update' => update('update', 'api'),
                );
                $array = array_merge($array, $arr);
            }
            if (isset($_GET['push']) && $_GET['push'] === 'main') {
                //add arr
                $arr = array(
                    'push' => submit('typecho', 'api'),
                );
                $array = array_merge($array, $arr);
            }
            if (isset($_GET['push']) && $_GET['push'] === 'all') {
                //add arr
                $arr = array(
                    'push' => submit('archive_all', 'api'),
                );
                $array = array_merge($array, $arr);
            }
            if (isset($_GET['push']) && $_GET['push'] === 'new') {
                //add arr
                $arr = array(
                    'push' => submit('archive', 'api'),
                );
                $array = array_merge($array, $arr);
            }
            echo json_encode($array);
        } else {
            //返回错误
            echo 'token error';
        }
    }

    public function sitemap_xml()
    {
        //检查更新时间
        $dir = __TYPECHO_ROOT_DIR__ . '/usr/log/sitemap';
        if (!file_exists($dir)) {
            update('activate', 'web');
            //返回xml
        }
        $XmlTime = filectime($dir);

        $ins = jkOptions::getInstance();
        $jkoptions = $ins::get_option( 'jkSiteHelper' );
        $cachetime = $jkoptions['seotools']['sitemap_cachetime'];
        $cachetime = intval($cachetime) * 86400;
        header('Cache-Control:max-age=' . $cachetime);
        header("Content-Type: application/xml");
        if (time() - $XmlTime > $cachetime) {
            update('update','auto');
        }

        $myfile = fopen($dir, "r");
        echo fread($myfile,filesize($dir));
        fclose($myfile);

    }
}


function update($function, $web)
{
    //更新sitemap.xml
    $db = \Typecho\Db::get();
    $options = \Typecho\Widget::widget('Widget_Options');
    $header = '<?xml version="1.0" encoding="utf-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . "\n";
    $footer = '</urlset>';
    //首页
    $index = Helper::options()->siteUrl;
    $index_result = '';
    $index_result = $index_result . "\t<url>\n";
    $index_result = $index_result . "\t\t<loc>" . $index . "</loc>\n";
    $index_result = $index_result . "\t\t<changefreq>always</changefreq>\n";
    $index_result = $index_result . "\t\t<priority>1</priority>\n";
    $index_result = $index_result . "\t</url>\n";
    //独立页面
    $pages = $db->fetchAll($db->select()->from('table.contents')
        ->where('table.contents.status = ?', 'publish')
        ->where('table.contents.created < ?', $options->gmtTime)
        ->where('table.contents.type = ?', 'page')
        ->order('table.contents.created', \Typecho\Db::SORT_DESC));
    $page_result = '';
    foreach ($pages as $page) {
        $type = $page['type'];
        $routeExists = (NULL != \Typecho\Router::get($type));
        $page['pathinfo'] = $routeExists ? \Typecho\Router::url($type, $page) : '#';
        $page['permalink'] = \Typecho\Common::url($page['pathinfo'], $options->index);
        $page_result = $page_result . "\t<url>\n";
        $page_result = $page_result . "\t\t<loc>" . $page['permalink'] . "</loc>\n";
        $page_result = $page_result . "\t\t<lastmod>" . date('Y-m-d', $page['modified']) . "</lastmod>\n";
        $page_result = $page_result . "\t\t<changefreq>always</changefreq>\n";
        $page_result = $page_result . "\t\t<priority>0.8</priority>\n";
        $page_result = $page_result . "\t</url>\n";
    }
    //分类
    $categorys = $db->fetchAll($db->select()->from('table.metas')
        ->where('table.metas.type = ?', 'category'));
    $category_result = '';
    foreach ($categorys as $category) {
        $type = $category['type'];
        $routeExists = (NULL != \Typecho\Router::get($type));
        $category['pathinfo'] = $routeExists ? \Typecho\Router::url($type, $category) : '#';
        $category['permalink'] = \Typecho\Common::url($category['pathinfo'], $options->index);
        $category_result = $category_result . "\t<url>\n";
        $category_result = $category_result . "\t\t<loc>" . $category['permalink'] . "</loc>\n";
        $category_result = $category_result . "\t\t<changefreq>always</changefreq>\n";
        $category_result = $category_result . "\t\t<priority>0.5</priority>\n";
        $category_result = $category_result . "\t</url>\n";
    }
    //文章
    $archives = $db->fetchAll($db->select()->from('table.contents')
        ->where('table.contents.status = ?', 'publish')
        ->where('table.contents.created < ?', $options->gmtTime)
        ->where('table.contents.type = ?', 'post')
        ->order('table.contents.created', \Typecho\Db::SORT_DESC));
    $archive_result = '';
    foreach ($archives as $archive) {
        $type = $archive['type'];
        $routeExists = (NULL != \Typecho\Router::get($type));
        $archive['pathinfo'] = $routeExists ? \Typecho\Router::url($type, $archive) : '#';
        $archive['permalink'] = \Typecho\Common::url($archive['pathinfo'], $options->index);
        $archive_result = $archive_result . "\t<url>\n";
        $archive_result = $archive_result . "\t\t<loc>" . $archive['permalink'] . "</loc>\n";
        $archive_result = $archive_result . "\t\t<lastmod>" . date('Y-m-d', $archive['modified']) . "</lastmod>\n";
        $archive_result = $archive_result . "\t\t<changefreq>always</changefreq>\n";
        $archive_result = $archive_result . "\t\t<priority>0.8</priority>\n";
        $archive_result = $archive_result . "\t</url>\n";
    }
    //tag
    $tags = $db->fetchAll($db->select()->from('table.metas')
        ->where('table.metas.type = ?', 'tag'));
    $tag_result = '';
    foreach ($tags as $tag) {
        $type = $tag['type'];
        $routeExists = (NULL != \Typecho\Router::get($type));
        $tag['pathinfo'] = $routeExists ? \Typecho\Router::url($type, $tag) : '#';
        $tag['permalink'] = \Typecho\Common::url($tag['pathinfo'], $options->index);
        $tag_result = $tag_result . "\t<url>\n";
        $tag_result = $tag_result . "\t\t<loc>" . $tag['permalink'] . "</loc>\n";
        $tag_result = $tag_result . "\t\t<changefreq>always</changefreq>\n";
        $tag_result = $tag_result . "\t\t<priority>0.5</priority>\n";
        $tag_result = $tag_result . "\t</url>\n";
    }
    $result = $header . $index_result . $page_result . $category_result . $archive_result . $tag_result . $footer; //xml内容
    $dir = __TYPECHO_ROOT_DIR__ . '/usr/log/sitemap';
    if ($function === 'activate') { //激活
        $myfile = fopen($dir, "w");
        fwrite($myfile, $result);
        fclose($myfile);
    }
    if ($function === 'update') { //更新
        if (file_exists($dir)){
            unlink($dir);
        }
        $myfile = fopen($dir, "w");
        fwrite($myfile, $result);
        fclose($myfile);
//        //获取时间
//        $time = date('Y-m-d H:i:s', time());
//        //写入日志
//        $log = '【' . $time . '】' . $web . '成功更新sitemap.xml' . "\n";
//        if (Helper::options()->plugin('Sitemap')->PluginLog == 1) {
//            file_put_contents(__TYPECHO_ROOT_DIR__ . __TYPECHO_PLUGIN_DIR__ . '/Sitemap/log.txt', $log, FILE_APPEND);
//        }
        //返回提示
        if ($web === 'web') {
            \Typecho\Widget::widget('Widget_Notice')->set(_t("更新 sitemap.xml 成功"), 'success');
        }
        if ($web === 'api') {
            return "success";
        }
    }
    return '';
}

// 被动推送
function submit($function, $web) //推送百度
{
    //检查baidu_url
    $ins = jkOptions::getInstance();
    $options = $ins::get_option( 'jkSiteHelper' );
    $push_url = $options['seotools']['baidu_push_url'];
    if ($push_url == NULL) {
        if ($web === 'web') {
            \Typecho\Widget::widget('Widget_Notice')->set(_t("请先设置百度站长平台的站点地址"), 'error');
            return;
        }
        if ($web === 'api') {
            return "BadiuApi is null";
        }
    }
    $db = \Typecho\Db::get();
    $options = \Typecho\Widget::widget('Widget_Options');
    if ($function === 'typecho') {
        //首页
        $index = Helper::options()->siteUrl;
        $urls = array($index);
        //独立页面
        $pages = $db->fetchAll($db->select()->from('table.contents')
            ->where('table.contents.status = ?', 'publish')
            ->where('table.contents.created < ?', $options->gmtTime)
            ->where('table.contents.type = ?', 'page')
            ->order('table.contents.created', \Typecho\Db::SORT_DESC));
        foreach ($pages as $page) {
            $type = $page['type'];
            $routeExists = (NULL != \Typecho\Router::get($type));
            $page['pathinfo'] = $routeExists ? \Typecho\Router::url($type, $page) : '#';
            $page['permalink'] = \Typecho\Common::url($page['pathinfo'], $options->index);
            array_push($urls, $page['permalink']);
        }
        //分类
        $categorys = $db->fetchAll($db->select()->from('table.metas')
            ->where('table.metas.type = ?', 'category'));
        foreach ($categorys as $category) {
            $type = $category['type'];
            $routeExists = (NULL != \Typecho\Router::get($type));
            $category['pathinfo'] = $routeExists ? \Typecho\Router::url($type, $category) : '#';
            $category['permalink'] = \Typecho\Common::url($category['pathinfo'], $options->index);
            array_push($urls, $category['permalink']);
        }
    }
    if ($function === 'archive' || $function === 'archive_all') {
        $urls = array();
        //文章
        $archives = $db->fetchAll($db->select()->from('table.contents')
            ->where('table.contents.status = ?', 'publish')
            ->where('table.contents.created < ?', $options->gmtTime)
            ->where('table.contents.type = ?', 'post')
            ->order('table.contents.created', \Typecho\Db::SORT_DESC));
        if ($function === 'archive') {
            //获取文章数量
            $stat = \Typecho\Widget::widget('Widget_Stat');
            $postnum = $stat->myPublishedPostsNum;
            if ($postnum >= 20) {
                for ($x = 0; $x < 20; $x++) {
                    $archive = $archives[$x];
                    $type = $archive['type'];
                    $routeExists = (NULL != \Typecho\Router::get($type));
                    $archive['pathinfo'] = $routeExists ? \Typecho\Router::url($type, $archive) : '#';
                    $archive['permalink'] = \Typecho\Common::url($archive['pathinfo'], $options->index);
                    array_push($urls, $archive['permalink']);
                }
            } else {
                if ($web === 'web') {
                    \Typecho\Widget::widget('Widget_Notice')->set(_t("文章小于20篇"), 'error');
                    return;
                }
                if ($web === 'api') {
                    return "文章小于20篇";
                }
            }
        }

        if ($function === 'archive_all') {
            foreach ($archives as $archive) {
                $type = $archive['type'];
                $routeExists = (NULL != \Typecho\Router::get($type));
                $archive['pathinfo'] = $routeExists ? \Typecho\Router::url($type, $archive) : '#';
                $archive['permalink'] = \Typecho\Common::url($archive['pathinfo'], $options->index);
                array_push($urls, $archive['permalink']);
            }
        }
    }
    $url_list = "";
    foreach ($urls as $url) {
        $url_list = $url_list . "\n" . $url;
    }

    $ch = curl_init();
    $options = array(
        CURLOPT_URL => $push_url,
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS => implode("\n", $urls),
        CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
    );
    curl_setopt_array($ch, $options);
    $result_json = curl_exec($ch);
    $result = json_decode($result_json, true);
    //获取时间
//    $time = date('Y-m-d H:i:s', time());
//    //写入日志
//    $log = '【' . $time . '】' . $web . '成功推送' . $result['success'] . '条，今日剩余可推送' . $result['remain'] . '条' . $url_list . "\n";
//    if (Helper::options()->plugin('Sitemap')->PluginLog == 1) {
//        file_put_contents(__TYPECHO_ROOT_DIR__ . __TYPECHO_PLUGIN_DIR__ . '/Sitemap/log.txt', $log, FILE_APPEND);
//    }
    //返回提示
    if ($web === 'web') {
        \Typecho\Widget::widget('Widget_Notice')->set(_t("推送完成"), 'success');
        //替换$url_list中的\n为<br>
        $url_list = str_replace("\n", "<br>", $url_list);
        return '成功推送' . $result['success'] . '条，今日剩余可推送' . $result['remain'] . '条' . "\n" . "\n" . "<p>" . $url_list . "</p>";
    }
    if ($web === 'api') {
        if (array_key_exists('error', $result)){
            return '错误！以下是百度官方返回的数据：错误码'.$result['error'].' '.$result['message'];
        }
        return "成功";
    }
}
