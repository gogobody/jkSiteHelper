<?php

namespace TypechoPlugin\jkSiteHelper\models\builtin\seotools;

use jkOptions;
use SEO_Baidu;
use SEO_Bing;
use SEO_SM;
use Typecho\Common;
use Utils\Helper;

define('BSL_URL',Common::url('/jkSiteHelper/models/builtin/seotools', Helper::options()->pluginUrl));
require_once 'classes/baidu.class.php';
require_once 'classes/bing.class.php';
require_once 'classes/sm.class.php';
require_once 'Action.php';
use TypechoPlugin\jkSiteHelper\models\builtin\seotools\Action;
use Widget\Options;

/**
 * seo 插件，代码参考：https://www.zhaoyingtian.com/archives/93.html
 */

class Plugin
{
    const Version = '1.0.0';
    public static function getNS(): string
    {
        return __NAMESPACE__;
    }
    public static function activate()
    {
        // 输出 各种 js 代码
        \Typecho\Plugin::factory('Widget_Archive')->header = [__CLASS__,'userhistorycode'];
        // 发布文章动作
        \Typecho\Plugin::factory('Widget_Contents_Post_Edit')->finishPublish = [__CLASS__, 'post2zhanzhang'];
        \Typecho\Plugin::factory('Widget_Contents_Page_Edit')->finishPublish = [__CLASS__, 'post2zhanzhang'];
        // 创建日志文件目录
        $log_dir = __TYPECHO_ROOT_DIR__ . '/usr/log/';
        if (!is_dir($log_dir)) @mkdir($log_dir, 0777);//检测缓存目录是否存在，自动创建
        // 添加路由
        Helper::addRoute('sitemap-api', '/sitemap-api', 'TypechoPlugin\jkSiteHelper\models\builtin\seotools\Action', 'sitemap_api');
        Helper::addRoute('sitemap', '/sitemap.xml', 'TypechoPlugin\jkSiteHelper\models\builtin\seotools\Action', 'sitemap_xml');
    }

    public static function deactivate()
    {
        // 是否需要清理日志
        Helper::removeRoute('sitemap-api');
        Helper::removeRoute('sitemap');
        $dir = __TYPECHO_ROOT_DIR__ . '/usr/log';
        delDir($dir);
    }

    public static function config($parent_id = null)
    {
        // 构造sitemap html
        $sitemap_html = '';
        $sitemap_url = Common::url('sitemap.xml', Options::alloc()->index);;
        $sitemap_api = Common::url('sitemap-api', Options::alloc()->index);;
        $ins = jkOptions::getInstance();
        $jkoptions = $ins::get_option( 'jkSiteHelper' );
        $api_token = jkOptions::getValue($jkoptions, 'seotools.api_token');

        $sitemap_html .= '<strong>sitemap地址</strong>：<a href="'.$sitemap_url.'">'.$sitemap_url.'</a><br><br>
<a href="'.$sitemap_api.'?token='.$api_token.'&sitemap=update" target="_blank" class="button button-primary csf--button">更新sitemap</a>  <a href="'.$sitemap_api.'?token='.$api_token.'&push=main" target="_blank" class="button button-primary csf--button">推送核心文章</a>
  <a href="'.$sitemap_api.'?token='.$api_token.'&push=all" target="_blank" class="button button-primary csf--button">推送全部文章</a>  <a href="'.$sitemap_api.'?token='.$api_token.'&push=new" target="_blank" class="button button-primary csf--button">推送新文章</a><br>提示：如果更新需要长时间请增加php脚本执行时间';

        $prefix = 'jkSiteHelper';
        \CSF::createSection($prefix, array(
                'parent' => $parent_id,
                'title' => 'SEO工具',
                'description' => '内置常用SEO工具，帮助你优化网站收录！',
                'fields' => array(
                    array(
                        'id' => 'seotools',
                        'type' => 'tabbed',
                        'tabs' => array(
                            array(
                                'title' => '常规设置',
                                'fields' => array(
                                    array(
                                        'id' => 'api_token',
                                        'type' => 'text',
                                        'title' => '路由访问秘钥',
                                        'after' => '随便填，主要是你自己用来外部更新sitemap。配置api token。用于更新sitemap。你可以设置宝塔定时访问此url来更新sitemap. '.$sitemap_api.'?sitemap=update&token=设置的token'
                                    ),
                                    array(
                                        'id' => 'sitemap_cachetime',
                                        'type' => 'text',
                                        'title' => 'Sitemap 缓存时间',
                                        'default' => '7',
                                        'after' => '单位（天）'
                                     ),
                                    array(
                                        'type'    => 'content',
                                        'content' => $sitemap_html,
                                    ),
                                    array(
                                        'id' => 'autoupdate',
                                        'type' => 'switcher',
                                        'title' => '发布文章自动推送',
                                        'default' => '1',
                                        'label' => '发布完推送百度，bing等。可能由于网络往返造成卡顿。'
                                    ),
                                    array(
                                        'id'      => 'post_type',
                                        'type'    => 'checkbox',
                                        'title'   => '推送链接类型',
                                        'inline'  => true,
                                        'options' => array(
                                            'post' => '文章',
                                            'page' => '页面'
                                        ),
                                        'default' => 'post',
                                    ),
                                    array(
                                        'id'      => 'enable_log',
                                        'type'    => 'switcher',
                                        'title'   => '启用推送日志',
                                        'default' => '1',
                                    ),
                                    array(
                                        'id' => 'tjdm',
                                        'type' => 'code_editor',
                                        'title' => '访客统计代码',
                                        'settings' => array(
                                            'theme'  => 'dracula',
                                            'mode'   => 'javascript',
                                        ),
                                        'after' => '例如百度统计，u盟等。<br>*: 注意不需要输入&lt;script&gt;标签'
                                    ),
//                                    array(
//                                        'id' => 'check_404',
//                                        'type' => 'switcher',
//                                        'title' => '死链检测设置',
//                                        'default' => false,
//                                        'label' => '*死链检测依赖Spider Analyser-蜘蛛分析插件。  *对死链及时处理有利于网站权重，建议开启该功能。'
//                                    ),

                                ),
                            ),
                            array(
                                'title' => '推送API',
                                'fields' => array(
                                    array(
                                        'id' => 'baidu_push_url',
                                        'type' => 'text',
                                        'title' => '百度接口调用地址',
                                        'after' => '填写普通收录或者快速收录任意一个API提交推送接口调用地址即可。<a href="https://www.wbolt.com/how-to-get-baidu-tokenid.html">查看教程</a>'
                                    ),
//                                    array(
//                                        'id' => 'bdsubmit_way',
//                                        'title' => '百度推送方式',
//                                        'type' => 'checkbox',
//                                        'options' => array(
//                                            'normal' => '普通收录主动推送',
//                                            'sitemap' => 'Sitemap地图推送'
//                                        ),
//                                        'default' => 'sitemap',
//                                        'after' => '温馨提示：</br>* 自动推送开关开启后，主题会添加自动推送工具代码，提高百度搜索引擎对站点新增网页发现速度。'
//                                    ),
                                    array(
                                        'id' => 'bing_key',
                                        'type' => 'text',
                                        'title' => 'Bing API密钥',
                                        'after' => '访问Bing网站管理员工具生成API密钥，<a href="https://www.wbolt.com/generate-bing-api-key.html">查看教程</a>'
                                    ),
                                    array(
                                        'id' => 'bingsubmit_way',
                                        'title' => 'Bing 推送方式',
                                        'type' => 'checkbox',
                                        'options' => array(
                                            'bing_manual' => '手动推送',
                                        ),
                                        'default' => 'bing_manual',
                                        'after' => '温馨提示：</br>* 务必先配置API密钥，否则无法正常推送。'
                                    ),
                                    array(
                                        'id' => 'qhjs',
                                        'type' => 'code_editor',
                                        'title' => '360推送 JS代码',
                                        'settings' => array(
                                            'theme'  => 'dracula',
                                            'mode'   => 'javascript',
                                        ),
                                        'after' => '访问登录360站长平台-数据提交-自动收录，获取JS代码。没有的话就是360暂时撤回了，不可用'
                                    ),
                                    array(
                                        'id' => 'sm_push_url',
                                        'type' => 'text',
                                        'title' => '神马推送 接口地址',
                                        'after' => '填写API提交推送接口调用地址即可。数据提交->MIP数据提交（复制接口调用地址）。'
                                    ),
                                    array(
                                        'id' => 'byte_js',
                                        'type' => 'code_editor',
                                        'title' => '头条推送 JS代码',
                                        'settings' => array(
                                            'theme'  => 'dracula',
                                            'mode'   => 'javascript',
                                        ),
                                        'after' => '访问站长平台-数据提交-自动收录，获取JS代码。'
                                    ),
                                ),
                            )
                        )
                    ),
                ),
            )
        );
    }

    public static function post2zhanzhang($contents, $class)
    {

        $options = Helper::options();

        //获取文章类型
        $type = $contents['type'];

        //获取路由信息
        $routeExists = \Typecho\Router::get($type);

        if (!is_null($routeExists)) {
            $db = \Typecho\Db::get();
            $contents['cid'] = $class->cid;
            $contents['categories'] = $db->fetchAll($db->select()->from('table.metas')
                ->join('table.relationships', 'table.relationships.mid = table.metas.mid')
                ->where('table.relationships.cid = ?', $contents['cid'])
                ->where('table.metas.type = ?', 'category')
                ->order('table.metas.order', \Typecho\Db::SORT_ASC));
            $contents['category'] = urlencode(current(array_column($contents['categories'], 'slug')));
            $contents['slug'] = urlencode($contents['slug']);
            $contents['date'] = new \Typecho\Date($contents['created']);
            $contents['year'] = $contents['date']->year;
            $contents['month'] = $contents['date']->month;
            $contents['day'] = $contents['date']->day;
        }

        //生成永久连接
        $path_info = $routeExists ? \Typecho\Router::url($type, $contents) : '#';
        $permalink = \Typecho\Common::url($path_info, $options->index);

        $ins = jkOptions::getInstance();
        $jkoptions = $ins::get_option( 'jkSiteHelper' );

        $autoupdate = $jkoptions['seotools']['autoupdate'];
        if ($autoupdate == '1'){
            SEO_Baidu::push($permalink);
            SEO_Bing::push($permalink);
            SEO_SM::push($permalink);
        }


    }

    // 插入统计代码
    public static function userhistorycode($header, $obj)
    {
        $ins = jkOptions::getInstance();
        $options = $ins::get_option( 'jkSiteHelper' );
        // 访客统计代码
        if (""!=$options['seotools']['tjdm']){
            echo '<script>'.$options['seotools']['tjdm'].'</script>';
        }
        // 奇虎360推送js
        if (""!=$options['seotools']['qhjs']){
            echo '<script>'.$options['seotools']['qhjs'].'</script>';
        }
        // 头条推送js
        if (""!=$options['seotools']['byte_js']){
            echo '<script>'.$options['seotools']['byte_js'].'</script>';
        }
    }
}

if (!function_exists('delDir')){
    function delDir($directory) { // 自定义函数递归的函数整个目录
        if (file_exists($directory)) { // 判断目录是否存在，如果不存在rmdir()函数会出错
            if ($dir_handle = @opendir($directory)) { // 打开目录返回目录资源，并判断是否成功
                while ($filename = readdir($dir_handle)) { // 遍历目录，读出目录中的文件或文件夹
                    if ($filename != '.' && $filename != '..') { // 一定要排除两个特殊的目录
                        $subFile = $directory . "/" . $filename; //将目录下的文件与当前目录相连
                        // echo $subFile . "<br>";
                        if (is_dir($subFile)) { // 如果是目录条件则成了
                            delDir($subFile); //递归调用自己删除子目录
                        }
                        if (is_file($subFile)) { // 如果是文件条件则成立
                            unlink($subFile); //直接删除这个文件
                        }
                    }
                }
                closedir($dir_handle); //关闭目录资源
                rmdir($directory); //删除空目录
            }
        }
    }
}

return __NAMESPACE__;
