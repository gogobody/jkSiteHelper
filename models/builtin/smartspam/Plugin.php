<?php
namespace TypechoPlugin\jkSiteHelper\models\builtin\smartspam;

use Widget\User;

/**
 * 评论过滤插件，参考 smartspam
 */
class Plugin
{
    const Version = '1.0.0';

    public static function getNS(): string
    {
        return __NAMESPACE__;
    }

    public static function activate()
    {
        \Typecho\Plugin::factory('Widget_Feedback')->comment = array(__CLASS__, 'filter');
        return _t('评论过滤器启用成功，请配置需要过滤的内容');
    }

    public static function config($parent_id = null)
    {
        $prefix = 'jkSiteHelper';
        \CSF::createSection($prefix, array(
            'parent'      => $parent_id,
            'title'       => '评论过滤',
            'description' => '智能评论过滤器，让机器人彻底远离你！',
            'fields'      => array(
                array(
                    'id'      => 'smartspam_enable',
                    'type'    => 'switcher',
                    'title'   => '评论过滤总开关',
                    'default' => true,
                ),
                array(
                    'id' => 'smartspam',
                    'type' => 'tabbed',
                    'tabs' => array(
                        array(
                            'title' => '评论',
                            'fields' => array(
                                array(
                                    'id'      => 'sspam-opt_login',
                                    'type'    => 'switcher',
                                    'title'   => '登录评论',
                                    'default' => false,
                                    'label' => '登录后才能评论',
                                ),
                                array(
                                    'id'      => 'sspam-opt_nojp',
                                    'type'    => 'radio',
                                    'title'   => '日文评论操作',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果评论中包含日文，则强行按该操作执行',
                                ),
                                array(
                                    'id'      => 'sspam-opt_nocn',
                                    'type'    => 'radio',
                                    'title'   => '非中文评论操作',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果评论中不包含中文，则强行按该操作执行',
                                ),
                                array(
                                    'id'       => 'sspam-length_min',
                                    'type'     => 'spinner',
                                    'title'    => '评论最短字符数',
                                    'subtitle' => '最小3，最大600',
                                    'max'      => 600,
                                    'min'      => 3,
                                    'step'     => 1,
                                    'default'  => 5,
                                ),
                                array(
                                    'id'       => 'sspam-length_max',
                                    'type'     => 'spinner',
                                    'title'    => '评论最长字符数',
                                    'subtitle' => '最小3，最大600，默认300',
                                    'max'      => 600,
                                    'min'      => 3,
                                    'step'     => 1,
                                    'default'  => 300,
                                ),
                                array(
                                    'id'      => 'sspam-opt_length',
                                    'type'    => 'radio',
                                    'title'   => '评论字符长度操作',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果评论中长度不符合条件，则强行按该操作执行。如果选择[无动作]，将忽略下面长度的设置',
                                ),

                                array(
                                    'id'    => 'sspam-words_ban',
                                    'type'  => 'textarea',
                                    'title' => '禁止词汇',
                                    'after' => '多条词汇请用换行符隔开',
                                    'default' => 'fuck\n操你妈\n[url\n[/url]'
                                ),
                                array(
                                    'id'      => 'sspam-opt_ban',
                                    'type'    => 'radio',
                                    'title'   => '禁止词汇操作',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果评论中包含禁止词汇列表中的词汇，将执行该操作',
                                ),
                                array(
                                    'id'    => 'sspam-words_chk',
                                    'type'  => 'textarea',
                                    'title' => '敏感词汇',
                                    'after' => '多条词汇请用换行符隔开。注意：如果词汇同时出现于禁止词汇，则执行禁止词汇操作',
                                    'default' => 'http://'
                                ),
                                array(
                                    'id'      => 'sspam-opt_chk',
                                    'type'    => 'radio',
                                    'title'   => '敏感词汇操作',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果评论中包含敏感词汇列表中的词汇，将执行该操作',
                                ),

                            ),
                        ),
                        array(
                            'title' => '昵称',
                            'fields' => array(
                                array(
                                    'id'      => 'sspam-opt_au',
                                    'type'    => 'radio',
                                    'title'   => '屏蔽昵称关键词操作',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果评论发布者的昵称含有该关键词，将执行该操作。',
                                ),
                                array(
                                    'id'      => 'sspam-words_au',
                                    'type'    => 'textarea',
                                    'title'   => '屏蔽昵称关键词',
                                    'after' => '多个关键词请用换行符隔开。',
                                ),

                                array(
                                    'id'       => 'sspam-au_length_min',
                                    'type'     => 'spinner',
                                    'title'    => '昵称最短字符数',
                                    'subtitle' => '最小1',
                                    'max'      => 100,
                                    'min'      => 1,
                                    'step'     => 1,
                                    'default'  => 3,
                                ),
                                array(
                                    'id'       => 'sspam-au_length_max',
                                    'type'     => 'spinner',
                                    'title'    => '昵称最长字符数',
                                    'subtitle' => '最小1，最大100',
                                    'max'      => 100,
                                    'min'      => 1,
                                    'step'     => 1,
                                    'default'  => 15,
                                ),
                                array(
                                    'id'      => 'sspam-opt_au_length',
                                    'type'    => 'radio',
                                    'title'   => '昵称字符长度操作',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果昵称长度不符合条件，则强行按该操作执行。如果选择[无动作]，将忽略下面长度的设置。',
                                ),
                                array(
                                    'id'      => 'sspam-opt_nojp_au',
                                    'type'    => 'radio',
                                    'title'   => '昵称日文操作',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果用户昵称中包含日文，则强行按该操作执行。',
                                ),
                                array(
                                    'id'      => 'sspam-opt_nourl_au',
                                    'type'    => 'radio',
                                    'title'   => '昵称网址操作',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果用户昵称是网址，则强行按该操作执行。',
                                ),
                            )
                        ),
                        array(
                            'title' => '其他',
                            'fields' => array(
                                array(
                                    'id'      => 'sspam-opt_ip',
                                    'type'    => 'radio',
                                    'title'   => '屏蔽IP操作',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果评论发布者的IP在屏蔽IP段，将执行该操作',

                                ),
                                array(
                                    'id'      => 'sspam-words_ip',
                                    'type'    => 'textarea',
                                    'title'   => '屏蔽IP',
                                    'after' => '多条IP请用换行符隔开<br />支持用*号匹配IP段，如：192.168.*.*',
                                ),

                                array(
                                    'id'      => 'sspam-opt_mail',
                                    'type'    => 'radio',
                                    'title'   => '屏蔽邮箱操作',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果评论发布者的邮箱与禁止的一致，将执行该操作',
                                ),
                                array(
                                    'id'      => 'sspam-words_mail',
                                    'type'    => 'textarea',
                                    'title'   => '邮箱关键词',
                                    'after' => '多个邮箱请用换行符隔开。可以是邮箱的全部，或者邮箱部分关键词',
                                ),

                                array(
                                    'id'      => 'sspam-opt_url',
                                    'type'    => 'radio',
                                    'title'   => '屏蔽网址操作',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果评论发布者的网址与禁止的一致，将执行该操作。如果网址为空，该项不会起作用。',
                                ),
                                array(
                                    'id'      => 'sspam-words_url',
                                    'type'    => 'textarea',
                                    'title'   => '网址关键词',
                                    'after' => '多个网址请用换行符隔开。可以是网址的全部，或者网址部分关键词。如果网址为空，该项不会起作用。',
                                ),

                                array(
                                    'id'      => 'sspam-opt_title',
                                    'type'    => 'radio',
                                    'title'   => '内容含有文章标题',
                                    'options' => array(
                                        'none' => '无动作',
                                        'waiting' => '标记为待审核',
                                        'spam' => '标记为垃圾',
                                        'abandon' => '评论失败',
                                    ),
                                    'default' => 'abandon',
                                    'after' => '如果评论内容中含有本页面的文章标题，则强行按该操作执行。',
                                ),

                            )
                        ),
                    ),
                    'dependency' => array('smartspam_enable', '==' ,'true')
                ),
            )
        ));
    }

    public static function filter($comment, $post)
    {
        if (!class_exists('CSF')) {
            require_once \Utils\Helper::options()->pluginDir('jkOptionsFramework') . '/jkoptions-framework.php';
        }
        $options = get_option('jkSiteHelper');
        $filter_set = $options['smartspam'];
        $opt = "none";
        $error = "";
        if ($options['sspam-opt_login'] == '1'){
            if (!User::alloc()->hasLogin()){
                $opt = "abandon";
                $error = "登录后才能评论";
            }
        }

        if ($options['smartspam_enable']!=='1'){
            return $comment;
        }


        //屏蔽评论内容包含文章标题
        if ($opt == "none" && $filter_set['sspam-opt_title'] != "none") {
            $db = \Typecho\Db::get();
            // 获取评论所在文章
            $po = $db->fetchRow($db->select('title')->from('table.contents')->where('cid = ?', $comment['cid']));
            if(strstr($comment['text'], $po['title'])){
                $error = "对不起，评论内容不允许包含文章标题";
                $opt = $filter_set['sspam-opt_title'];
            }
        }


        //屏蔽IP段处理
        if ($opt == "none" && $filter_set['sspam-opt_ip'] != "none") {
            if (self::check_ip($filter_set['sspam-words_ip'], $comment['ip'])) {
                $error = "评论发布者的IP已被管理员屏蔽";
                $opt = $filter_set['sspam-opt_ip'];
            }
        }


        //屏蔽邮箱处理
        if ($opt == "none" && $filter_set['sspam-opt_mail'] != "none") {
            if (self::check_in($filter_set['sspam-words_mail'], $comment['mail'])) {
                $error = "评论发布者的邮箱地址被管理员屏蔽";
                $opt = $filter_set['sspam-opt_mail'];
            }
        }

        //屏蔽网址处理
        if(!empty($filter_set['sspam-words_url'])){
            if ($opt == "none" && $filter_set['sspam-opt_url'] != "none") {
                if (self::check_in($filter_set['sspam-words_url'], $comment['url'])) {
                    $error = "评论发布者的网址被管理员屏蔽";
                    $opt = $filter_set['sspam-opt_url'];
                }
            }
        }


        //屏蔽昵称关键词处理
        if ($opt == "none" && $filter_set['sspam-opt_au'] != "none") {
            if (self::check_in($filter_set['sspam-words_au'], $comment['author'])) {
                $error = "对不起，昵称的部分字符已经被管理员屏蔽，请更换";
                $opt = $filter_set['sspam-opt_au'];
            }
        }


        //日文评论处理
        if ($opt == "none" && $filter_set['sspam-opt_nojp'] != "none") {
            if (preg_match("/[\x{3040}-\x{31ff}]/u", $comment['text']) > 0) {
                $error = "禁止使用日文";
                $opt = $filter_set['sspam-opt_nojp'];
            }
        }


        //日文用户昵称处理
        if ($opt == "none" && $filter_set['sspam-opt_nojp_au'] != "none") {
            if (preg_match("/[\x{3040}-\x{31ff}]/u", $comment['author']) > 0) {
                $error = "用户昵称禁止使用日文";
                $opt = $filter_set['sspam-opt_nojp_au'];
            }
        }


        //昵称长度检测
        if ($opt == "none" && $filter_set['sspam-opt_au_length'] != "none") {
            if(self::strLength($comment['author']) < $filter_set['sspam-au_length_min']){
                $error = "昵称请不得少于".$filter_set['sspam-au_length_min']."个字符";
                $opt = $filter_set['sspam-opt_au_length'];
            }else
                if(self::strLength($comment['author']) >  $filter_set['sspam-au_length_max']){
                    $error = "昵称请不得多于".$filter_set['sspam-au_length_max']."个字符";
                    $opt = $filter_set['sspam-opt_au_length'];
                }

        }

        //用户昵称网址判断处理
        if ($opt == "none" && $filter_set['sspam-opt_nourl_au'] != "none") {
            if (preg_match(" /^((https?|ftp|news):\/\/)?([a-z]([a-z0-9\-]*[\.。])+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel)|(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))(\/[a-z0-9_\-\.~]+)*(\/([a-z0-9_\-\.]*)(\?[a-z0-9+_\-\.%=&]*)?)?(#[a-z][a-z0-9_]*)?$/ ", $comment['author']) > 0) {
                $error = "用户昵称不允许为网址";
                $opt = $filter_set['sspam-opt_nourl_au'];
            }
        }


        //纯中文评论处理
        if ($opt == "none" && $filter_set['sspam-opt_nocn'] != "none") {
            if (preg_match("/[\x{4e00}-\x{9fa5}]/u", $comment['text']) == 0) {
                $error = "评论内容请不少于一个中文汉字";
                $opt = $filter_set['sspam-opt_nocn'];
            }
        }


        //字符长度检测
        if ($opt == "none" && $filter_set['sspam-opt_length'] != "none") {
            if(self::strLength($comment['text']) < $filter_set['length_min']){
                $error = "评论内容请不得少于".$filter_set['length_min']."个字符";
                $opt = $filter_set['sspam-opt_length'];
            }else
                if(self::strLength($comment['text']) >  $filter_set['sspam-length_max']){
                    $error = "评论内容请不得多于".$filter_set['sspam-length_max']."个字符";
                    $opt = $filter_set['sspam-opt_length'];
                }

        }

        //检查禁止词汇
        if ($opt == "none" && $filter_set['sspam-opt_ban'] != "none") {
            if (self::check_in($filter_set['sspam-words_ban'], $comment['text'])) {
                $error = "评论内容中包含禁止词汇";
                $opt = $filter_set['sspam-opt_ban'];
            }
        }
        //检查敏感词汇
        if ($opt == "none" && $filter_set['sspam-opt_chk'] != "none") {
            if (self::check_in($filter_set['sspam-words_chk'], $comment['text'])) {
                $error = "评论内容中包含敏感词汇";
                $opt = $filter_set['sspam-opt_chk'];
            }
        }

        //执行操作
        if ($opt == "abandon") {
            \Typecho\Cookie::set('__typecho_remember_text', $comment['text']);
            throw new \Typecho\Widget\Exception($error);
        }
        else if ($opt == "spam") {
            $comment['status'] = 'spam';
        }
        else if ($opt == "waiting") {
            $comment['status'] = 'waiting';
        }
        \Typecho\Cookie::delete('__typecho_remember_text');
        return $comment;
    }

    /**
     * PHP获取字符串中英文混合长度
     */
    private static function strLength($str){
        preg_match_all('/./us', $str, $match);
        return count($match[0]);  // 输出9
    }


    /**
     * 检查$str中是否含有$words_str中的词汇
     *
     */
    private static function check_in($words_str, $str)
    {
        if (empty($words_str)) return false;

        $words = explode("\n", $words_str);
        if (empty($words)) {
            return false;
        }
        foreach ($words as $word) {
            if (false !== strpos($str, trim($word))) {
                return true;
            }
        }
        return false;
    }

    /**
     * 检查$ip中是否在$words_ip的IP段中
     *
     */
    private static function check_ip($words_ip, $ip)
    {
        if (empty($words_ip)) return false;

        $words = explode("\n", $words_ip);
        if (empty($words)) {
            return false;
        }
        foreach ($words as $word) {
            $word = trim($word);
            if (false !== strpos($word, '*')) {
                $word = "/^".str_replace('*', '\d{1,3}', $word)."$/";
                if (preg_match($word, $ip)) {
                    return true;
                }
            } else {
                if (false !== strpos($ip, $word)) {
                    return true;
                }
            }
        }
        return false;
    }
}

return __NAMESPACE__;
