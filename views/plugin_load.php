<?php

use Utils\Helper;

$typecho_jq = Helper::options()->adminStaticUrl('js', 'jquery.js', true);
$action_url = \Typecho\Common::url('download_jk_file', Helper::options()->index);
$build_plugins = \TypechoPlugin\jkSiteHelper\classes\SiteHelper::$plugins;
$p_v_string = "";
foreach ($build_plugins as $key => $val){
    $plugin_name_arr = explode('\\', $key); // TypechoPlugin\jkSiteHelper\models\builtin\seotools\Plugin 所以pop两次
    array_pop($plugin_name_arr);
    $plugin_name = array_pop($plugin_name_arr);
    $version = $val['version'];
    $p_v_string.="'$plugin_name':'$version',";
}
echo "<script>const jkSiteHelper={plugins:{ $p_v_string }}</script>";

echo "<link rel='stylesheet' href='".jksite_baseurl."/assets/css/loading.min.css'><script src='".jksite_baseurl."/assets/js/loading.min.js'></script>
<script src='".jksite_baseurl."/assets/js/base64.min.js'></script>
<script src='".$typecho_jq."'></script>
<script>
const load = new Loading({type: 1,tipLabel: '加载中',wrap: document.getElementsByClassName('jksite-loading')[0]});
function jk_setplugin_content(data) {
    var stnode = $('#standalone_tools');
    var otnode = $('#outer-tools');
    var otools = data.outer_tools;
    var stools = data.standalone_tools;
    if (otools){
        var ohtml = '';
        otools.forEach(function ( val, index, arr){
            var option = '';
            if (val.plugin_name in jkSiteHelper.plugins){
                var curversion=jkSiteHelper.plugins[val.plugin_name];
                if (curversion < val.version){
                   option = '已安装:'+jkSiteHelper.plugins[val.plugin_name]+'<br>有新版本'+'<button type=\"button\" onclick=\"download_jkplugin(\''+val.download_url+'\')\">下载</button>';
                } else {
                   option = '已安装:'+jkSiteHelper.plugins[val.plugin_name];
                }
            } else {
               option = '<button type=\"button\" onclick=\"download_jkplugin(\''+val.download_url+'\')\">下载</button>';
            }
            ohtml = ohtml + '<tr><td>'+parseInt(index+1)+'</td><td><a target=\"_blank\" href=\"'+val.url+'\">'+val.title+'</a></td><td>'+val.desc+'</td><td>'+option+'</td></tr>'
        });otnode.html(ohtml);
    }
    if (stools){
        var shtml = '';
        stools.forEach(function ( val, index, arr){
            var option = '前往下载';
            shtml = shtml + '<tr><td>'+parseInt(index+1)+'</td><td><a target=\"_blank\" href=\"'+val.url+'\">'+val.title+'</a></td><td>'+val.desc+'</td><td><a href=\"'+val.url+'\" target=\"_blank\">'+option+'</a></td></tr>'
        });stnode.html(shtml);
    }
}
function download_jkplugin(url) {
  var load = new Loading({type: 3,tipLabel: '下载中...请适当增加php脚本执行时间',wrap: document.getElementsByClassName('jksite-loading')[0]});load.init();
  $.post('".$action_url."',{url:url},function(res) {
      console.log(res)
    if (res.errno === 0){
        alert('下载成功，请 禁用->启用 jkSiteHelper！');load.hide();
    } else {
        alert(res.message)
    }
  })
}
function check_jkUpdate() {
    var curmins = parseInt(Date.parse(new Date())/60); // 当前分钟数
    $.get('https://gitee.com/api/v5/repos/gogobody/jkSiteHelper/contents/data/version.json',function (res){load.hide();
        if (res.encoding === 'base64'){
            var data = JSON.parse(Base64.decode(res.content));
            jk_setplugin_content(data);
            localStorage.setItem('sitehelper_content',JSON.stringify({
            time:curmins,content:data
            }))
        } else {
            alert('gitee 解码错误，请通知管理员')
        }
    })
}
$(document).ready(function (){
    load.init();
    var curmins = parseInt(Date.parse(new Date())/60); // 当前分钟数
    var oldcontent = localStorage.getItem('sitehelper_content');
    var need_load = true;
    if (oldcontent){
        oldcontent = JSON.parse(oldcontent);
        if (curmins - parseInt(oldcontent.time) < 10 ){
            jk_setplugin_content(oldcontent.content);need_load=false;
        }
    } 
    if (need_load){
        check_jkUpdate()
    }else {load.hide();}
})
</script>";
